package uz.osg.spring.blog.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
public class BlogController {

    @PostMapping("/comment")
    @ResponseStatus(HttpStatus.OK)
    public String comment(@RequestParam String comment) {
        return comment + " " + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
    }
}
