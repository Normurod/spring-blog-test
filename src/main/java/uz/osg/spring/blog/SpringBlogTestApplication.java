package uz.osg.spring.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBlogTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBlogTestApplication.class, args);
	}

}
